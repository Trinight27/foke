<?php
session_start();
include ("requeteSQL.php");
getConnexion();
?>
<!doctype html>
<?php

?>

<html lang="fr">
    <body>
        <header>
            <h1>Bienvenue chez les chatons tout mignons</h1>
        </header>
        <main>
            <ul>
                <li><a href="listechatons.html.php">Afficher la liste des chatons</a></li>
                <li><a href="ajoutchaton.html.php">Créer un nouveau chaton</a></li>
                <li><a href="suppressionchaton.html.php">Supprimer un chaton</a></li>
                <li><a href="combatchaton.php">Lancer un combat</a></li>
            </ul>
            <div>
                <a class="style" href="#">Le nombre de combat est affiché en bas à droite de votre écran</a>
            </div>
        </main>
        <?php include("bilan.php"); ?>
    </body>
</html>
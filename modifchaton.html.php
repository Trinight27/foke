<?php
session_start();
?>

<!doctype html>

<?php

include("fonctions.php");
include ("requeteSQL.php");
if(!isset($_SESSION['lesChatons'])){
    $lesChatons = getLesChatonsBDD();
    $_SESSION['lesChatons'] = $lesChatons;
}else{
    $lesChatons = $_SESSION['lesChatons'];
}


?>

<<html lang="fr">
<body>
<header>
    <h2>
        <?php
        if (isset($_GET['nom'])) {
            modifChatonBDD($_GET['nom'],$_GET['nouvNom'],(int)$_GET['pouvoir']);
        }

        $lesPouvoirs = getLesPouvoirsBDD();
        ?>
    </h2>
    <h1>Modification d'un chaton</h1>
</header>
<main>
    <form action="modifchaton.html.php" method="get">
        <div>
            <label for="name">Nom du chaton:</label>
            <input type="text" id="name" name="nom">

            <div>
                <label for="newName">Nouveau nom du chaton :</label>
                <input type="text" id="newName" name="nouvNom">

            </div>
            <div>
                <label for="pouvoir_id">Nouveau pouvoir</label> :
                <select name="pouvoir" id="pouvoir_id">
                    <?php
                    foreach ($lesPouvoirs as $pouv => $id){
                        echo ("<option value =".$id.">".$pouv."<option>");
                    }
                    ?>

                </select>
            </div>
            <button type="submit">Envoyer</button>
        </div>
    </form>
    <br/>
    <a href="index.html.php">Retour au menu</a>
</main>
<?php include("bilan.php"); ?>
</body>
</html>
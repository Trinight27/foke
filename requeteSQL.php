<?php
include("connexionBDD.php");

function getLesChatonsBDD()
{
    $result = pg_query(getConnexion(), "SELECT nom FROM chaton ");
    if (!$result) {
        echo "Une erreur s'est produite.\n";
        exit;
    }
    $leschatons = [];
    while ($row = pg_fetch_row($result)) {
        array_push($leschatons, $row[0]);
    }
    return $leschatons;


}


function afficheLesChatonsBDD()
{
    $result = pg_query(getConnexion(), "SELECT nom,libelle FROM chaton inner join pouvoir on chaton.idpouvoir = pouvoir.id");
    if (!$result) {
        echo "Une erreur s'est produite.\n";
        exit;
    }

    while ($row = pg_fetch_row($result)) {
        echo "Nom : $row[0] - Son pouvoir : $row[1]";
        echo "<br />\n";
    }
}

function getLesPouvoirsBDD()
{
    $result = pg_query(getConnexion(), "SELECT libelle,id FROM pouvoir ");
    if (!$result) {
        echo "Une erreur s'est produite.\n";
        exit;
    }
    $lespouvoirs = [];
    while ($row = pg_fetch_row($result)) {
        array_push($lespouvoirs, $row[0]);
        $lespouvoirs[$row[0]] = $row[1];

    }
    return $lespouvoirs;

}

function addChatonBDD($nom, $pouvoir)
{

    pg_query(getConnexion(), "INSERT INTO chaton (nom,idpouvoir) VALUES ('$nom',$pouvoir) ");
}


function supChatonBDD($leChaton)
{
    if (pg_query(getConnexion(),"SELECT nom FROM chaton WHERE EXISTS (SELECT chaton.nom FROM chaton WHERE '$leChaton' = nom)") == true){

        pg_query(getConnexion(), "DELETE FROM chaton WHERE '$leChaton' = nom ");
        $msg = "Vous avez abandonné " . $leChaton;

    }
    else{
        $msg = "Aucun chaton de ce nom n'a été trouvé ";

    }

    echo $msg;
}

function modifChatonBDD($leChaton,$nouvNom,$idPouvoir){

    if (pg_query(getConnexion(),"SELECT nom FROM chaton WHERE EXISTS (SELECT chaton.nom FROM chaton WHERE '$leChaton' = nom)") == true){

        pg_query(getConnexion(), "UPDATE chaton SET nom='$nouvNom', idpouvoir= '$idPouvoir' WHERE '$leChaton' = nom ");

        $msg = $leChaton." s'appelle maintenant ".$nouvNom." et à changé de pouvoir";

    }
    else{
        $msg = "Aucun chaton de ce nom n'a été trouvé ";

    }
    

    echo $msg;
}
<?php
session_start();
?>
<!doctype html>
<?php
    include("fonctions.php");
    include ("requeteSQL.php");
    $lesPouvoirs = getLesPouvoirsBDD();
?>

<html lang="fr">
    <body>
        <main>
            <form action="listechatons.html.php" method="get">
                <div>
                    <label for="name">Nom du chaton:</label>
                    <input type="text" id="name" name="nom">
                </div>
                <div>
                    <label for="pouvoir_id">Pouvoir</label> :
                    <select name="pouvoir" id="pouvoir_id">
                        <?php
                        foreach ($lesPouvoirs as $pouv => $id){
                            echo ("<option value =".$id.">".$pouv."<option>");
                        }
                        ?>

                    </select>
                </div>
                <div>
                    <button type="submit">Enregistrer</button>
                </div>
            </form>
            <br/>
            <a href="index.html.php">Retour au menu</a>
        </main>
        <?php include("bilan.php"); ?>
    </body>
</html>
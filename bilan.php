<!doctype html>
<?php
    if(!isset($_SESSION['bilan'])) {
        $_SESSION['bilan'] = 0;
    }
?>

<html lang="fr">
    <body>
        <footer>
            <div>
                <?php echo 'Nombre de combat : '.$_SESSION['bilan']?>
            </div>
        </footer>
        <style>
            footer div{
                position: absolute;
                right: 7px;
                bottom: 7px;
            }

            div a{
                display: block;
                color: black;
                text-align: center;
                text-decoration: none;
            }

            div a:visited.style{
                color:white;
            }
        </style>
    </body>
</html>
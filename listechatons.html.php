<?php
session_start();
?>
<!doctype html>
<?php

    include("fonctions.php");
    include ("requeteSQL.php");
    if(!isset($_SESSION['lesChatons'])){
        $lesChatons = getLesChatonsBDD();
        $_SESSION['lesChatons'] = $lesChatons;
    }else{
        $lesChatons = $_SESSION['lesChatons'];
    }

    if(isset($_GET['nom'])){
        addChatonBDD($_GET['nom'],$_GET['pouvoir']);
        $lesChatons[$_GET['nom']] = $_GET['pouvoir'];
        $_SESSION['lesChatons'] = $lesChatons;
    }

?>

<html lang="fr">
    <body>
        <header>
            <h1>Liste des chatons :</h1>
        </header>
        <main>
            <?php
//                afficherLesChatons($lesChatons);
                afficheLesChatonsBDD();

            ?>
            <br/>
            <div>
                <button onclick="location.href='modifchaton.html.php'" type="button">Modifier un chaton !!!</button>
            </div>
            <br/>
            <a href="index.html.php">Retour au menu</a>
        </main>
        <?php include("bilan.php"); ?>
    </body>
</html>
<?php
session_start();
?>
<!doctype html>
<?php
    include("fonctions.php");
    include ("requeteSQL.php");
    if(!isset($_SESSION['lesChatons'])){
        $lesChatons = getLesChatonsBDD();
        $_SESSION['lesChatons'] = $lesChatons;
    }else{
        $lesChatons = $_SESSION['lesChatons'];
    }

?>

<html lang="fr">
    <body>
        <header>
            <h2>
                <?php
                    if (isset($_GET['nom'])) {
                        supChatonBDD($_GET['nom']);
                    }
                ?>
            </h2>
            <h1>Abandon d'un chaton</h1>
        </header>
        <main>
            <form action="suppressionchaton.html.php" method="get">
                <div>
                    <label for="name">Nom du chaton:</label>
                    <input type="text" id="name" name="nom">
                    <button type="submit">Envoyer</button>
                </div>
            </form>
            <br/>
            <a href="index.html.php">Retour au menu</a>
        </main>
        <?php include("bilan.php"); ?>
    </body>
</html>
<?php
session_start();
?>
<!doctype html>
<?php
    include("fonctions.php");
    include ("requeteSQL.php");
    if(!isset($_SESSION['bilan'])) {
        $_SESSION['bilan'] = 0;
    }

    if(!isset($_SESSION['lesChatons'])){
        $lesChatons = array("Isidore" => "feu", "Rif raf" => "eau", "Brutus" => "terre", "Daphne" => "feu");
        $_SESSION['lesChatons'] = $lesChatons;
    }else{
        $lesChatons = $_SESSION['lesChatons'];
    }


?>

<html lang="fr">
    <body>
        <header>
            <h2>
                <?php
                    if(isset($_GET['combattant1'])){
                        $_SESSION['bilan'] += 1;
                        combatChatons($lesChatons);
                    }
                ?>
            </h2>
            <h1>Lancement du combat de chatons</h1>
        </header>
        <main>
            <form action="combatchaton.php" method="get">
                <div>
                    <label for="combattant1_id"></label>
                    <select name="combattant1" id="combattant1_id">
                        <?php
                            foreach($lesChatons as $key => $value){
                                echo '<option>' . $key . '</option>';
                            }
                        ?>
                    </select>
                    <label for="combattant2_id">VERSUS</label>
                    <select name="combattant2" id="combattant2_id">
                        <?php
                            foreach($lesChatons as $key => $value){
                                echo '<option>' . $key . '</option>';
                            }
                        ?>
                    </select>
                    <button type="submit">GO</button>
                </div>
            </form>
            <br/>
            <a href="index.html.php">Retour au menu</a>
        </main>
        <?php include("bilan.php"); ?>
    </body>
</html>